import {quill, getDelimiterPositionsList, charIndexToCharIndexBounds} from './quill.js'
import search from './search.js'
import { isValid } from './CheckDef.js'

/* working variables */
var start_index, stop_index;
var sentence_boundary;
var player; 
var next_delimiter;
var interrupt;
var handle;
var defaultPlayerRate;
var playerReadMode;
var Timer_Running;
var Timer_handle;
var reactionTimeQuantum;
var locationContext = [];
var feedback; 
var transitionTime;

var audioEndOfRead = document.getElementById("audioEndOfRead")

function init() {
    start_index = 0, stop_index = 0; 
    sentence_boundary = [],
    next_delimiter = 0;
    interrupt = false;
    playerReadMode = 'burst'
    Timer_Running = false;
    reactionTimeQuantum = 3700; // in ms
    transitionTime = 0;

    clearTimeout(handle);
    clearTimeout(Timer_handle);

}

/* Speech synthesizer setup — Talkify */
export function setup() {
    /* Authorize Talkify */
    talkify.config.remoteService.host = 'https://talkify.net';
    talkify.config.remoteService.apiKey = '76d17af9-81c4-4b5a-8143-c14200d3fcd1';

    talkify.config.ui.audioControls = {
        enabled: true, //<-- Disable to get the browser built in audio controls
        container: document.getElementById("player-and-voices")
    };

    player = new talkify.TtsPlayer();
    player.forceVoice({name: 'David'});
    player.subscribeTo({
        onPause:function(){
            if ( !interrupt && !feedback && playerReadMode == 'burst' ) {
                if ( next_delimiter < sentence_boundary.length )
                    loop();
                else if ( next_delimiter == sentence_boundary.length )      {}
                    // audioEndOfRead.play()
            }
            else if (interrupt)     interrupt   = false;
            else if (feedback)      feedback    = false;

        }
    });

    feedback = false;
    defaultPlayerRate = -2;
}

export function readSelection(selection_index) {
    console.log('raw index to start reading (burst mode) from :: ', selection_index)
    var content = quill.getText().trim()
    
    if ( !isValid(selection_index) || selection_index > content.length) {
        // audioEndOfRead.play();
        return;
    }

    /* get indices of all sentence delimiters within whole text */
    init()
    sentence_boundary = getDelimiterPositionsList()

    if (selection_index == 0)
        start_index = 0
    else
        start_index = content.lastIndexOf(' ', selection_index) + 1; //index of text


    console.log('processed index to start reading (burst mode) from :: ', start_index)

    setLocationContext(start_index, 'char')

    next_delimiter = search(sentence_boundary, start_index) //iterator of sentence_boundary array
    stop_index = sentence_boundary[next_delimiter++] // stops at the delimiter [.?!]
    console.log('start_index', start_index, ' :: ', 'stop_index', stop_index)

    read(content.slice(start_index, stop_index+1));
}

function loop(index) {
    /* 
        start a 'reaction-time' Timer for 1 second. if paused or given instruction 
        before the Timer expires then still stay within the context of the last read sentence. 
    */
    console.log('starting reaction timer')
    transitionTime = new Date().getTime();
    Timer_Running = true;
    Timer_handle = setTimeout(StopTimer, reactionTimeQuantum)

    start_index = stop_index + 1
    stop_index = sentence_boundary[next_delimiter++]
    console.log('start_index', start_index, ' :: ', 'stop_index', stop_index)
    console.log('reading line', quill.getText().slice(start_index, stop_index+1))
    read(quill.getText().slice(start_index, stop_index+1));
}

function StopTimer() {
    Timer_Running = false; 
    console.log('reaction timer expired')
    console.log('transition time :: ', new Date().getTime() - transitionTime)
    clearTimeout(Timer_handle);
}

export function read(text, rate, feedbackReq) {
    feedbackReq = feedbackReq || undefined
    rate = rate || defaultPlayerRate; 

    if (feedbackReq)
        feedback = true

    player.setRate(rate).playText(text)
}

export function readSentence(lbound) {
    playerReadMode = 'single'
    feedback = false;
    var content = quill.getText().trim()

    if ( !isValid(lbound) || lbound > content.length ) {
        // audioEndOfRead.play();
        return;
    }

    if (lbound <= 0)
        lbound = 0
    else if (quill.getText(lbound-1, 1) !== ' ')
        lbound = content.lastIndexOf(' ', lbound) + 1; // char index of start of word
    
    console.log('lbound = ', lbound)
    
    setLocationContext(lbound, 'char')  // this is character index -> needs to be converted to sent index

    var ubound = charIndexToCharIndexBounds(lbound).ubound

    read(quill.getText(lbound, ubound-lbound+1))
}


export function pause() {
    interrupt = true
    player.pause();

    if (playerReadMode == 'burst') {
        if (Timer_Running) 
            setLocationContext(next_delimiter -2)
        else 
            setLocationContext(next_delimiter -1)
    }
}

export function isPlaying() {
    if (player.isPlaying()) 
        return true
    return false
}

export function getReadMode() {
    return playerReadMode
}

export function getLocationContext() {
    if (locationContext.length == 0)
        return undefined

    return parseInt(locationContext.slice(-1));
}

export function setLocationContext(value, indexType) {
    indexType = indexType || 'sent' // by default sentence index

    if(indexType === 'sent')
        locationContext.push(value);
    else if (indexType === 'char')  // convert char index to sent index
        locationContext.push( search( getDelimiterPositionsList(), value ) )
        
    console.log('Location Context Stack = ', locationContext)
}

