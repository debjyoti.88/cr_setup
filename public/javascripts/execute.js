import * as intent from './operations.js'

// audiocons
var audioInsert = document.getElementById("audioInsert")
var audioDelete = document.getElementById("audioDelete")
var audioChange = document.getElementById("audioChange")
var audioError = document.getElementById("audioError")
var audioUndoRedo = document.getElementById("audioUndoRedo")

var resumeDelay = 250 // in ms

let playAudio = function() {
    return new Promise(function(resolve) {
        $("#audioInsert").bind("ended", function() {
            setTimeout( () => {
                console.log('audio played and delay added')
                resolve();
            }, resumeDelay)
        });
    
        $("#audioDelete").bind("ended", function() {
            setTimeout( () => {
                resolve();
            }, resumeDelay)
        });

        $("#audioChange").bind("ended", function() {
            setTimeout( () => {
                resolve();
            }, resumeDelay)
        });

        $("#audioError").bind("ended", function() {
            setTimeout( () => {
                resolve();
            }, resumeDelay)
        });

    })
}

export default function(operation) {
    console.log('executing operation')
    return new Promise(function(resolve) {

        if (operation.status == 'error') {
            audioError.play()
            Promise.all([playAudio()]).then(function() {
                intent.error(operation.errorMessage.text, operation.errorMessage.rate, 'true') //feedbackReq = true
                resolve();
            })
        }

        else {
            let updateQuill;
            switch (operation.intent) {
                case 'insert':
                    updateQuill = function() {
                        return new Promise(function(resolve) {
                            intent.insert(operation.pos, operation.slot, operation.formatString).then(function() {
                                resolve();
                            }) 
                        })
                    }
                    audioInsert.play()
                    Promise.all([updateQuill(), playAudio()]).then(function() {
                        resolve();
                    })
                    break;
        
                case 'delete':
                    updateQuill = function() {
                        return new Promise(function(resolve) {
                            intent.remove(operation.pos, operation.nChar).then(function() {
                                resolve();
                            }) 
                        })
                    }
                    audioDelete.play()
                    Promise.all([updateQuill(), playAudio()]).then(function() {
                        resolve();
                    })
                    break;

                case 'change':
                    updateQuill = function() {
                        return new Promise(function(resolve) {
                            intent.replace(operation.pos, operation.nChar, operation.slot, operation.formatString).then(function() {
                                resolve();
                            }) 
                        })
                    }
                    audioChange.play()
                    Promise.all([updateQuill(), playAudio()]).then(function() {
                        resolve();
                    })
                    break;
        
                case 'read':
                case 'repeat':
                    intent.performRead(operation.lbound, operation.readMode)
                    break;

                case 'undo':
                case 'redo':
                    updateQuill = function() {
                        return new Promise(function(resolve) {
                            intent.undo_redo(operation.intent).then(function() {
                                resolve();
                            }) 
                        })
                    }
                    audioUndoRedo.play()
                    Promise.all([updateQuill(), playAudio()]).then(function() {
                        resolve();
                    })
                    break;

        
            }
        }

    })

}


