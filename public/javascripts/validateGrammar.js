const validOrderedPairs = [    // TAG1, LABEL1, TAG2, LABEL2
    ['ADJ', '', 'NOUN', ''], 
    ['ADV', '', 'ADJ', ''],
    ['', 'NUM', 'NOUN', ''],
    ['', 'NUM', '', 'NUM'],
    ['', 'NN', 'NOUN', ''],
    ['', 'POSS', 'NOUN', ''],
    ['VERB', '', '', 'PRT' ],
    ['VERB', '', '', 'CC' ],
    ['NOUN', '', '', 'CC' ],
    ['', 'CC', '', 'NOUN' ],
    ['', 'CC', 'DET', '' ],
    ['', 'CC', 'DET', '' ],
    ['', 'CC', '', 'CONJ'],
    ['DET', '', 'NOUN', ''],
    ['DET', '', 'ADJ', ''],
    ['DET', '', 'ADV', ''],
    ['VERB', '', 'DET', ''],
    ['VERB', '', '', 'ACOMP'],
    ['NOUN', '', 'VERB', '']    // experimental  
]

export const isValid2Gram = (token1, token2) => {
    var tag1 = token1.tag, 
        tag2 = token2.tag; 

    var label1 = token1.label, 
        label2 = token2.label;

    if (tag1 && tag2)
        return validOrderedPairs.findIndex( x => tag1 == x[0] && tag2 == x[2] )

    else if (tag1 && label2)
        return validOrderedPairs.findIndex( x => tag1 == x[0] && label2 == x[3] )

    else if (label1 && tag2)
        return validOrderedPairs.findIndex( x => label1 == x[1] && tag2 == x[2] )

    else if (label1 && label2)
        return validOrderedPairs.findIndex( x => label1 == x[1] && label2 == x[3] )

    return -1; 

}