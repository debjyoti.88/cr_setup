export default function (tokenString) {
    tokenString = preprocess(tokenString)
    // Return a new promise.
    return new Promise(function(resolve, reject) {
      // Do the usual XHR stuff
      var req = new XMLHttpRequest();
      var url = "http://phrasefinder.io/search?corpus=eng-us&query=" + tokenString + "&topk=1"
      console.log('url', url)
      req.open('GET', url);
      // req.withCredentials = true;
      req.setRequestHeader('Content-Type', 'text/plain');

      req.onload = function() {
        // This is called even on 404 etc
        // so check the status
        if (req.status == 200) {
          // Resolve the promise with the response text
          resolve(req.response);
        }
        else {
          // Otherwise reject with the status text
          // which will hopefully be a meaningful error
          reject(Error(req.statusText));
        }
      };
  
      // Handle network errors
      req.onerror = function() {
        reject(Error("Network Error"));
      };
  
      // Make the request
      req.send();
    });
  }
  
  function preprocess(str) {
    var re = /\d+/g;
    var match

    while ( (match = re.exec(str)) !== null)
      str = str.replace(match, numberToWords.toWords(match))
    return str
  }