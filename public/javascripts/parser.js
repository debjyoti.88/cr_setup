/* Intent Matcher module */ 

import { findTarget, gettext, charIndexToCharIndexBounds, sentIndexToCharIndexBounds, getLeftContext, getRightContext, findTargetFromBoundaryString, getResumeAtCharIndex, getAPIOperationIndex, quill, getSentenceAtLocationContext } from './quill.js'
import { error } from './error.js'
import { read, getLocationContext, getReadMode } from './tts.js'
import { socket, getSelection } from './main.js'
import { longestIncreasingSequence } from './lis.js'
import { isValid } from './CheckDef.js';
import * as overlap from './overlap.js'
import { cartesian } from './cartesian.js'
import { isValid2Gram } from './validateGrammar.js';
import queryLM from './LM.js'

var query = "", 
    context = {},       //returns object with both content and location
    slot = "";

var formatString = 'default';
var resObj, deixisObj;

const dynamicErrorMessageRate = 1;
const staticErrorMessageRate = 3;
var selectREDICT;
var selectKEYWORD;
const PENALTY_FACTOR = 5; 
const LM_SCORE_THRESHOLD = 5000;

// here data.transcript from main is passed as data
export function parse(data, locationContext) {   // data: {utterance: , intent: , params: }
    // unload response from dialogflow server to local vars
    var insert = data.params.insert,
        remove = data.params.remove,
        change = data.params.change,
        resume = data.params.resume,
        read_ = data.params.read,
        undo = data.params.undo,
        redo = data.params.redo;


    var rel_loc = data.params.rel_target_loc,
        chunk = data.params.chunk,
        abs_loc = data.params.abs_target_loc,
        query = data.params.query,
        target = data.params.target,
        container = data.params.container_phrases,
        granularity = data.params.granularity,
        target_deixis = data.params.target_deixis,
        batch_ind = data.params.batch_ind,
        terminal_ind = data.params.terminal_ind,
        del_phrase = data.params.delPhrase,
        container_target = data.params.container_target,
        repeat = data.params.repeat,
        deixis = data.params.deixis;

    return new Promise(function(resolve) {

        // if (gettext() == "")
        //     resolve({intent: 'default'})

        var keyword = (read_ && 'read') || (resume && 'resume') || (repeat && 'repeat') || (insert && 'insert') || (remove && 'remove') || (change && 'change') || (undo && 'undo') || (redo && 'redo');
        console.log('Keyword Identified :: ', keyword)
        console.log('Intent Matched To :: ', data.intent)

        // const isEmpty = Object.values(data.params).every(x => (x === ''));
        var operation; 

        if ( !keyword && !del_phrase && !deixis && !repeat && data.intent !== 'getLocation' && data.intent !== 'stop' ) 
            operation = 'default'
        else
            operation = keyword || data.intent

        console.log('Operation decided :: ', operation)

        // operation switch
        var techSelect = getSelection('technique')
        selectKEYWORD = false;
        selectREDICT = false;

        switch( techSelect ) {
            case 0:
                selectKEYWORD = true;
                break;
            case 1:
                selectREDICT = true;
                break;
            case 2:
                break;
            default:
                // alert('Please select a Technique.')
                operation = 'default';
        }

        switch( true ) {

            case (!selectREDICT && operation === 'insert'):
                // if (selectREDICT)
                //     resolve(
                //         {
                //             status: 'error', 
                //             errorMessage: {
                //                 text: error()['KEYWORD_NOT_ALLOWED'],
                //                 rate: staticErrorMessageRate
                //             }
                //         }
                //     )

                if (!keyword)
                    resolve(
                        {
                            status: 'error', 
                            errorMessage: {
                                text: error(
                                    {param: 'insert'}
                                )['KEYWORD_NOT_FOUND'],
                                rate: dynamicErrorMessageRate
                            }
                        }
                    )

                if (!query && !chunk)
                    resolve(
                        {
                            status: 'error', 
                            errorMessage: {
                                text: error(
                                    {param: 'insert'}
                                )['SLOT_NOT_FOUND'],
                                rate: dynamicErrorMessageRate
                            }
                        }
                    )

                var targetLocation = -1;
                
                if (target || chunk) {

                    if (target) {
                        targetLocation = findTarget(target, locationContext); 

                        if (targetLocation == -1) {
                            if (!target && chunk)
                                target = chunk.split(' ')[0]
        
                            resolve(
                                {
                                    status: 'error', 
                                    errorMessage: {
                                        text: error(
                                            {target}
                                        )['TARGET_NOT_FOUND'],
                                        rate: dynamicErrorMessageRate
                                    }
                                }
                            )
                        }
                    }

                    else {      // for chunk
                        var l_context = getLeftContext(chunk, locationContext);
                        targetLocation = l_context.location;

                        if (targetLocation == -1) {
                            if (!target && chunk)
                                target = chunk.split(' ')[0]
        
                            resolve(
                                {
                                    status: 'error', 
                                    errorMessage: {
                                        text: error(
                                            {target}
                                        )['TARGET_NOT_FOUND'],
                                        rate: dynamicErrorMessageRate
                                    }
                                }
                            )
                        }
        
                        if (container && !container_target) {   // eg. "having the words bla bla.. in it"
                            container_target = l_context.content
                            query = chunk.substring(container_target.length + 1)
                            var bounds = charIndexToCharIndexBounds(targetLocation);
                            resObj = navigateDir(query, bounds, rel_loc, abs_loc);
                            
                            // unload
                            targetLocation = resObj.targetLocation
                            query = resObj.query
                            formatString = resObj.formatString
                        }

                        else {
                            target =  l_context.content
                            query = chunk.substring(target.length + 1)
                        }
                    }

                    
                    if (rel_loc == 'after') {
                        targetLocation += target.length +1;

                        if (quill.getText(targetLocation-1, 1) === '.') { // if query needs to be inserted just before the delimiter
                            query = ' ' + query;
                            --targetLocation;
                            formatString = 'newline';
                        }
                    }

                    if ( !query )
                        resolve(
                            {
                                status: 'error', 
                                errorMessage: {
                                    text: error(
                                        {param: 'insert'}
                                    )['SLOT_NOT_FOUND'],
                                    rate: dynamicErrorMessageRate
                                }
                            }
                        )

                    resolve(
                        {
                            intent: 'insert',
                            pos: targetLocation,
                            slot: query,
                            formatString,
                            ttsResumeAt: getResumeAtCharIndex(targetLocation)
                        }
                    )
                        
                }

                else if ( target_deixis || ( !target_deixis && (abs_loc || (rel_loc && granularity)) ) ) {

                    if ( target_deixis && (terminal_ind || container) ) {

                        if (container_target) {

                            if (terminal_ind) {
                                targetLocation = findTargetFromBoundaryString(container_target, locationContext, terminal_ind);
                                
                                if (targetLocation == -1)
                                    resolve(
                                        {
                                            status: 'error', 
                                            errorMessage: {
                                                text: error(
                                                    {param: terminal_ind.abs_target_loc, container_target}
                                                )['MATCH_AT_BOUNDARY_NOT_FOUND'],
                                                rate: dynamicErrorMessageRate
                                            }
                                        }
                                    )
                            }
                            
                            else if (container) {      // container_phrases eg. line having bla bla.. 
                                targetLocation = findTarget(container_target, locationContext);
                                
                                if (targetLocation == -1)
                                    resolve(
                                        {
                                            status: 'error', 
                                            errorMessage: {
                                                text: error(
                                                    {container_target}
                                                )['CONTAINER_TARGET_NOT_FOUND'],
                                                rate: dynamicErrorMessageRate
                                            }
                                        }
                                    )
                            }

                            else
                                resolve(
                                    {
                                        status: 'error', 
                                        errorMessage: {
                                            text: error(
                                                {param: 'insert'}
                                            )['OPERATION_FAILED'],
                                            rate: dynamicErrorMessageRate
                                        }
                                    }
                                )

                        }
        
                        deixisObj = charIndexToCharIndexBounds(targetLocation)
                        deixisObj.lbound = deixisObj.lbound + 2

                    }

                    else {
                        if ( !target_deixis && (abs_loc || (rel_loc && granularity)) )
                            target_deixis = 'this'
                    
                        deixisObj = parseDeixis(target_deixis, locationContext, 'insert'); 

                        if(deixisObj.error)
                            resolve(
                                {
                                    status: 'error', 
                                    errorMessage: {
                                        text: error(
                                            {param: deixisObj.param}
                                        )[deixisObj.error],
                                        rate: dynamicErrorMessageRate
                                    }
                                }
                            )

                        resObj = navigateDir(query, deixisObj, rel_loc, abs_loc)

                        if ( !resObj.query )
                            resolve(
                                {
                                    status: 'error', 
                                    errorMessage: {
                                        text: error(
                                            {param: 'insert'}
                                        )['SLOT_NOT_FOUND'],
                                        rate: dynamicErrorMessageRate
                                    }
                                }
                            )

                        resolve(
                            {
                                intent: 'insert',
                                pos: resObj.targetLocation,
                                slot: resObj.query,
                                formatString: resObj.formatString,
                                ttsResumeAt: getResumeAtCharIndex(resObj.targetLocation)
                            }
                        )
                    }

                }

                else
                    resolve(
                        {
                            status: 'error', 
                            errorMessage: {
                                text: error(
                                    {param: 'insert'}
                                )['OPERATION_FAILED'],
                                rate: dynamicErrorMessageRate
                            }
                        }
                    )

            
                break;

        
            case (!selectREDICT && operation === 'remove'):
                if (gettext() == "")
                    resolve(
                        {
                            status: 'error', 
                            errorMessage: {
                                text: error()['BLANK_QUILL'],
                                rate: staticErrorMessageRate
                            }
                        }
                    )

                // if (selectREDICT)
                //     resolve(
                //         {
                //             status: 'error', 
                //             errorMessage: {
                //                 text: error()['KEYWORD_NOT_ALLOWED'],
                //                 rate: staticErrorMessageRate
                //             }
                //         }
                //     )

                if (!keyword && !del_phrase.keyword)
                    resolve(
                        {
                            status: 'error', 
                            errorMessage: {
                                text: error(
                                    {param: 'delete'}
                                )['KEYWORD_NOT_FOUND'],
                                rate: dynamicErrorMessageRate
                            }
                        }
                    )

                var targetLocation = -1, nChar = 0;

                if (target) {       // lazy approach -> for sentences with both target & container target doesn't validate for container_target. 
                    targetLocation = findTarget(target, locationContext);

                    if (targetLocation == -1)
                        resolve(
                            {
                                status: 'error', 
                                errorMessage: {
                                    text: error(
                                        {target}
                                    )['TARGET_NOT_FOUND'],
                                    rate: dynamicErrorMessageRate
                                }
                            }
                        )

                    nChar = target.length + 1;
                    if (quill.getText(targetLocation + target.length, 1) === '.')
                        --targetLocation;

                    resolve(
                        {
                            intent: 'delete',
                            pos: targetLocation,
                            nChar,
                            ttsResumeAt: getResumeAtCharIndex(targetLocation)
                        }
                    ) 

                }

                else if (container_target) {    // sentence level deletion
                    if (terminal_ind) {
                        targetLocation = findTargetFromBoundaryString(container_target, locationContext, terminal_ind);
                        
                        if (targetLocation == -1)
                            resolve(
                                {
                                    status: 'error', 
                                    errorMessage: {
                                        text: error(
                                            {param: terminal_ind.abs_target_loc, container_target}
                                        )['MATCH_AT_BOUNDARY_NOT_FOUND'],
                                        rate: dynamicErrorMessageRate
                                    }
                                }
                            )
                    }
                    else {      // container_phrases eg. line having bla bla.. 
                        targetLocation = findTarget(container_target, locationContext);
                        
                        if (targetLocation == -1)
                            resolve(
                                {
                                    status: 'error', 
                                    errorMessage: {
                                        text: error(
                                            {container_target}
                                        )['CONTAINER_TARGET_NOT_FOUND'],
                                        rate: dynamicErrorMessageRate
                                    }
                                }
                            )
                    }

                    resObj = charIndexToCharIndexBounds(targetLocation)
                    targetLocation = resObj.lbound + 2
                    nChar = resObj.ubound - resObj.lbound +1 + ' '.length
                    
                    resolve(
                        {
                            intent: 'delete',
                            pos: targetLocation,
                            nChar,
                            ttsResumeAt: getResumeAtCharIndex(targetLocation)
                        }
                    )
                    
                }

                else if (target_deixis || del_phrase.target_deixis) {   // target_deixis but no target, eg. "erase the last sentence"
                    if (!target_deixis && del_phrase.target_deixis)
                        target_deixis = del_phrase.target_deixis

                    deixisObj = parseDeixis(target_deixis, locationContext, 'delete'); 

                    if(deixisObj.error)
                        resolve(
                            {
                                status: 'error', 
                                errorMessage: {
                                    text: error(
                                        {param: deixisObj.param}
                                    )[deixisObj.error],
                                    rate: dynamicErrorMessageRate
                                }
                            }
                        )
                        
                    resolve(
                        {
                            intent: 'delete',
                            pos: deixisObj.lbound,
                            nChar: deixisObj.ubound - deixisObj.lbound +1 + ' '.length,
                            ttsResumeAt: getResumeAtCharIndex(deixisObj.lbound)
                        }
                        
                    )

                }

                else
                    resolve(
                        {
                            status: 'error', 
                            errorMessage: {
                                text: error(
                                    {param: 'delete'}
                                )['OPERATION_FAILED'],
                                rate: dynamicErrorMessageRate
                            }
                        }
                    )

                break;


            case (!selectREDICT && operation === 'change'):
                if (gettext() == "")
                    resolve(
                        {
                            status: 'error', 
                            errorMessage: {
                                text: error()['BLANK_QUILL'],
                                rate: staticErrorMessageRate
                            }
                        }
                    )

                // if (selectREDICT)
                //     resolve(
                //         {
                //             status: 'error', 
                //             errorMessage: {
                //                 text: error()['KEYWORD_NOT_ALLOWED'],
                //                 rate: staticErrorMessageRate
                //             }
                //         }
                //     )

                if (!keyword)
                    resolve(
                        {
                            status: 'error', 
                            errorMessage: {
                                text: error(
                                    {param: 'replace'}
                                )['KEYWORD_NOT_FOUND'],
                                rate: dynamicErrorMessageRate
                            }
                        }
                    )

                if (!query)
                    resolve(
                        {
                            status: 'error', 
                            errorMessage: {
                                text: error(
                                    {param: 'replace'}
                                )['SLOT_NOT_FOUND'],
                                rate: dynamicErrorMessageRate
                            }
                        }
                    )

                var targetLocation = -1, nChar = 0;

                if (target || chunk) {       // lazy approach -> for sentences with both target & container target doesn't validate for container_target. 

                    if (target)
                        targetLocation = findTarget(target, locationContext);
                    
                    else if (chunk) {
                        var l_context = getLeftContext(chunk, locationContext);
                        targetLocation = l_context.location;
                        container_target =  l_context.content
                        target = chunk.substring(container_target.length + 1)
                    }

                    if (targetLocation == -1) {
                        if (target == '')
                            target = chunk.split(' ')[0]

                        resolve(
                            {
                                status: 'error', 
                                errorMessage: {
                                    text: error(
                                        {target}
                                    )['TARGET_NOT_FOUND'],
                                    rate: dynamicErrorMessageRate
                                }
                            }
                        )
                    }

                    nChar = target.length + 1;
                    if (quill.getText(targetLocation + target.length, 1) === '.')
                        --targetLocation;

                    resolve(
                        {
                            intent: 'change',
                            pos: targetLocation,
                            nChar,
                            slot: query,
                            formatString: 'default',
                            ttsResumeAt: getResumeAtCharIndex(targetLocation)
                        }
                    ) 

                }

                else if (container_target) {    // sentence level replacement

                    if (terminal_ind) {
                        targetLocation = findTargetFromBoundaryString(container_target, locationContext, terminal_ind);
                        
                        if (targetLocation == -1)
                            resolve(
                                {
                                    status: 'error', 
                                    errorMessage: {
                                        text: error(
                                            {param: terminal_ind.abs_target_loc, container_target}
                                        )['MATCH_AT_BOUNDARY_NOT_FOUND'],
                                        rate: dynamicErrorMessageRate
                                    }
                                }
                            )
                    }
                    
                    else if (container) {      // container_phrases eg. line having bla bla.. 
                        targetLocation = findTarget(container_target, locationContext);
                        
                        if (targetLocation == -1)
                            resolve(
                                {
                                    status: 'error', 
                                    errorMessage: {
                                        text: error(
                                            {container_target}
                                        )['CONTAINER_TARGET_NOT_FOUND'],
                                        rate: dynamicErrorMessageRate
                                    }
                                }
                            )
                    }

                    else
                        resolve(
                            {
                                status: 'error', 
                                errorMessage: {
                                    text: error(
                                        {param: 'replace'}
                                    )['OPERATION_FAILED'],
                                    rate: dynamicErrorMessageRate
                                }
                            }
                        )

                    resObj = charIndexToCharIndexBounds(targetLocation)
                    targetLocation = resObj.lbound + 2
                    nChar = resObj.ubound - resObj.lbound +1 + ' '.length
                    
                    resolve(
                        {
                            intent: 'change',
                            pos: targetLocation,
                            nChar,
                            slot: query,
                            formatString: 'changeline',
                            ttsResumeAt: getResumeAtCharIndex(targetLocation)
                        }
                    )
                    
                }

                else if (target_deixis) {   // target_deixis but no target, eg. "change the last sentence to "You are welcome"
                    deixisObj = parseDeixis(target_deixis, locationContext, 'replace'); 

                    if(deixisObj.error)
                        resolve(
                            {
                                status: 'error', 
                                errorMessage: {
                                    text: error(
                                        {param: deixisObj.param}
                                    )[deixisObj.error],
                                    rate: dynamicErrorMessageRate
                                }
                            }
                        )
                        
                    resolve(
                        {
                            intent: 'change',
                            pos: deixisObj.lbound,
                            nChar: deixisObj.ubound - deixisObj.lbound +1 + ' '.length,
                            slot: query,
                            formatString: 'changeline',
                            ttsResumeAt: getResumeAtCharIndex(deixisObj.lbound)
                        }
                        
                    )

                }

                else
                    resolve(
                        {
                            status: 'error', 
                            errorMessage: {
                                text: error(
                                    {param: 'replace'}
                                )['OPERATION_FAILED'],
                                rate: dynamicErrorMessageRate
                            }
                        }
                    )

                break;


            case (operation === 'read'):
            case (operation === 'resume'):
                if (gettext() == "")
                    resolve(
                        {
                            status: 'error', 
                            errorMessage: {
                                text: error()['BLANK_QUILL'],
                                rate: staticErrorMessageRate
                            }
                        }
                    )

                if (!keyword && !deixis)
                    resolve(
                        {
                            status: 'error', 
                            errorMessage: {
                                text: error(
                                    {param: 'read'}
                                )['KEYWORD_NOT_FOUND'],
                                rate: dynamicErrorMessageRate
                            }
                        }
                    )
                
                var readMode = 'single';
                var lbound = 0, bounds;

                if (deixis) {
                    deixisObj = parseDeixis(data.params, locationContext, 'read'); 

                    if(deixisObj.error)
                        resolve(
                            {
                                status: 'error', 
                                errorMessage: {
                                    text: error(
                                        {param: deixisObj.param}
                                    )[deixisObj.error],
                                    rate: dynamicErrorMessageRate
                                }
                            }
                        )

                    resolve(
                        {
                            intent: 'read',
                            lbound: deixisObj.lbound,
                            readMode
                        }
                    )
        
                }

                else if (container_target) {
                    if (terminal_ind) {
                        targetLocation = findTargetFromBoundaryString(container_target, locationContext, terminal_ind);
                        
                        if (targetLocation == -1)
                            resolve(
                                {
                                    status: 'error', 
                                    errorMessage: {
                                        text: error(
                                            {param: terminal_ind.abs_target_loc, container_target}
                                        )['MATCH_AT_BOUNDARY_NOT_FOUND'],
                                        rate: dynamicErrorMessageRate
                                    }
                                }
                            )

                        lbound = targetLocation
                    }

                    else if (container) {
                        targetLocation = findTarget(container_target, locationContext);
                        
                        if (targetLocation == -1)
                            resolve(
                                {
                                    status: 'error', 
                                    errorMessage: {
                                        text: error(
                                            {container_target}
                                        )['CONTAINER_TARGET_NOT_FOUND'],
                                        rate: dynamicErrorMessageRate
                                    }
                                }
                            )

                        console.log(`"${container_target}" found at ${targetLocation}`) 
                        console.log(`lower bound of enclosing sentence at ${targetLocation}`) 
                    }

                    var bounds = charIndexToCharIndexBounds(targetLocation);
                    bounds.lbound = bounds.lbound + 2;
                    
                    if (container)
                        lbound = bounds.lbound
                    
                    if (target_deixis.rel_target_loc) 
                        resObj = navigateDir(query, bounds, rel_loc, abs_loc);
                        lbound = resObj.lbound

                    if (batch_ind || keyword == 'resume')
                        readMode = 'burst'

                    resolve(
                        {
                            intent: 'read',
                            lbound,
                            readMode
                        }
                    )
                }
                    
                else if (target_deixis) {
                    deixisObj = parseDeixis(target_deixis, locationContext, 'read'); 

                    if(deixisObj.error)
                        resolve(
                            {
                                status: 'error', 
                                errorMessage: {
                                    text: error(
                                        {param: deixisObj.param}
                                    )[deixisObj.error],
                                    rate: dynamicErrorMessageRate
                                }
                            }
                        )
                    
                    lbound = deixisObj.lbound

                    if (abs_loc || rel_loc) {
                        resObj = navigateDir(query, deixisObj, rel_loc, abs_loc)
                        lbound = resObj.lbound
                    }

                    if (batch_ind)
                        readMode = 'burst'

                    resolve(
                        {
                            intent: 'read',
                            lbound,
                            readMode
                        }
                    )

                } 

                else if (batch_ind && abs_loc == 'beginning')
                    resolve(
                        {
                            intent: 'read',
                            lbound: 0,
                            readMode: 'burst'
                        }
                    )

                else 
                    resolve(
                        {
                            intent: 'read',
                            lbound: sentIndexToCharIndexBounds(getLocationContext()).this +2,  // read from next line
                            readMode: 'burst'
                        }
                    )
                    
                break;


            case (operation === 'repeat'):
                if (gettext() == "")
                    resolve(
                        {
                            status: 'error', 
                            errorMessage: {
                                text: error()['BLANK_QUILL'],
                                rate: staticErrorMessageRate
                            }
                        }
                    )

                if (!repeat)
                    resolve(
                        {
                            status: 'error', 
                            errorMessage: {
                                text: error(
                                    {param: 'repeat'}
                                )['KEYWORD_NOT_FOUND'],
                                rate: dynamicErrorMessageRate
                            }
                        }
                    )
                
                var readMode;
                var lbound = 0, bounds;

                if (container_target) {
                    if (terminal_ind) {
                        targetLocation = findTargetFromBoundaryString(container_target, locationContext, terminal_ind);
                        
                        if (targetLocation == -1)
                            resolve(
                                {
                                    status: 'error', 
                                    errorMessage: {
                                        text: error(
                                            {param: terminal_ind.abs_target_loc, container_target}
                                        )['MATCH_AT_BOUNDARY_NOT_FOUND'],
                                        rate: dynamicErrorMessageRate
                                    }
                                }
                            )

                        lbound = targetLocation
                    }

                    else if (container) {
                        targetLocation = findTarget(container_target, locationContext);
                        
                        if (targetLocation == -1)
                            resolve(
                                {
                                    status: 'error', 
                                    errorMessage: {
                                        text: error(
                                            {container_target}
                                        )['CONTAINER_TARGET_NOT_FOUND'],
                                        rate: dynamicErrorMessageRate
                                    }
                                }
                            )

                        console.log(`"${container_target}" found at ${targetLocation}`) 
                        console.log(`lower bound of enclosing sentence at ${targetLocation}`) 
                    }

                    var bounds = charIndexToCharIndexBounds(targetLocation);
                    bounds.lbound = bounds.lbound + 2;
                    
                    if (container)
                        lbound = bounds.lbound
                    
                    if (target_deixis.rel_target_loc) 
                        resObj = navigateDir(query, bounds, rel_loc, abs_loc);
                        lbound = resObj.lbound

                    if (batch_ind)
                        readMode = 'burst'

                    resolve(
                        {
                            intent: 'repeat',
                            lbound,
                            readMode: readMode || getReadMode()
                        }
                    )
                }
                    
                else if (target_deixis && !repeat.deixis) {
                    deixisObj = parseDeixis(target_deixis, locationContext, 'repeat'); 

                    if(deixisObj.error)
                        resolve(
                            {
                                status: 'error', 
                                errorMessage: {
                                    text: error(
                                        {param: deixisObj.param}
                                    )[deixisObj.error],
                                    rate: dynamicErrorMessageRate
                                }
                            }
                        )
                    
                    lbound = deixisObj.lbound

                    if (abs_loc || rel_loc) {
                        resObj = navigateDir(query, deixisObj, rel_loc, abs_loc)
                        lbound = resObj.lbound
                    }

                    if (batch_ind)
                        readMode = 'burst'

                    resolve(
                        {
                            intent: 'repeat',
                            lbound,
                            readMode: readMode || getReadMode()
                        }
                    )

                } 

                else if (batch_ind && abs_loc == 'beginning')
                    resolve(
                        {
                            intent: 'repeat',
                            lbound: 0,
                            readMode: 'burst'
                        }
                    )

                else {
                    console.log('repeat object :: ', repeat)

                    if (repeat.target_deixis || repeat.deixis) {
                        if (repeat.deixis)
                            repeat.target_deixis = {deixis: repeat.deixis}
                        
                        deixisObj = parseDeixis(repeat.target_deixis, getLocationContext(), 'repeat'); 
                        console.log('deixisObj in repeat :: ', deixisObj)
                        if(deixisObj.error)
                            resolve(
                                {
                                    status: 'error', 
                                    errorMessage: {
                                        text: error(
                                            {param: deixisObj.param}
                                        )[deixisObj.error],
                                        rate: dynamicErrorMessageRate
                                    }
                                }
                            )
                        
                        lbound = deixisObj.lbound
                        console.log('lbound in repeat :: ', lbound)
                    }

                    else
                        lbound = sentIndexToCharIndexBounds(getLocationContext()).prev +2

                    resolve(
                        {
                            intent: 'repeat',
                            lbound: lbound || 0,
                            readMode: getReadMode()
                        }
                    )

                }

                break;
        

            case (operation === 'stop'):    // this is intent and not keyword
                document.getElementById('editor').click();
                break;


            case (operation === 'getLocation'):    // this is intent and not keyword
                if (gettext() == "")
                    resolve(
                        {
                            status: 'error', 
                            errorMessage: {
                                text: error()['BLANK_QUILL'],
                                rate: staticErrorMessageRate
                            }
                        }
                    )
                
                var responseText;
                var findMe = getLocationContext() + 1
                if (findMe == 0)
                    responseText = 'You are at the beginning of the text'
                else
                    responseText = data.response + ' ' + findMe;
                console.log(responseText)
                read(responseText, staticErrorMessageRate, 'true')
                
                break;


            case (operation === 'undo'):
                if (!keyword)
                    resolve(
                        {
                            status: 'error', 
                            errorMessage: {
                                text: error(
                                    {param: 'undo'}
                                )['KEYWORD_NOT_FOUND'],
                                rate: dynamicErrorMessageRate
                            }
                        }
                    )

                else if (keyword == 'undo') {
                    resolve(
                        {
                            intent: keyword,
                            ttsResumeAt: getResumeAtCharIndex(getAPIOperationIndex())
                        }
                    )
                }

                else {
                    resolve(
                        {
                            status: 'error', 
                            errorMessage: {
                                text: error(
                                    {param: 'undo'}
                                )['OPERATION_FAILED'],
                                rate: dynamicErrorMessageRate
                            }
                        }
                    )
                }
                    
                break;

            case (operation === 'redo'):
                if (!keyword)
                    resolve(
                        {
                            status: 'error', 
                            errorMessage: {
                                text: error(
                                    {param: 'redo'}
                                )['KEYWORD_NOT_FOUND'],
                                rate: dynamicErrorMessageRate
                            }
                        }
                    )

                else if (keyword == 'redo') {
                    resolve(
                        {
                            intent: keyword,
                            ttsResumeAt: getResumeAtCharIndex(getAPIOperationIndex())
                        }
                    )
                }

                else {
                    resolve(
                        {
                            status: 'error', 
                            errorMessage: {
                                text: error(
                                    {param: 'redo'}
                                )['OPERATION_FAILED'],
                                rate: dynamicErrorMessageRate
                            }
                        }
                    )
                }
                    
                break;

            default:
                if ( !selectKEYWORD )
                    sendPacketToNLPServer(data.utterance, locationContext)
                else {
                    console.log('Default :: No Operation')
                    resolve(
                        {
                            status: 'error', 
                            errorMessage: {
                                text: error(
                                    {param: data.utterance}
                                )['NO_INTENT_MATCHED'],
                                rate: dynamicErrorMessageRate
                            }
                        }
                    )
                }

        }       // end of switch

    })      // end of promise
}        


const parseDeixis = function(deixis, locationContext, caller) {
    console.log('inside parseDeixis :: ', deixis)
    if (deixis !== 'this' && !deixis.granularity && !deixis.number && !deixis.deixis) {
        return {error: 'REF_NOT_FOUND', param: caller};
    }

    var ordinal = deixis.ordinal, 
        number  = deixis.number;

    var thisSentIndex = ordinal?ordinal-1:(number?number-1:locationContext);
    var thisObj = sentIndexToCharIndexBounds(thisSentIndex) // bounds for either this sentence or sentence whose ordinal is specified

    if (!thisObj.this)
        return {error: 'SENTENCE_INDEX_OUT_OF_BOUND', param: thisObj.count};

    if ( deixis.deixis == 'previous' || (deixis.granularity == 'sentence' && deixis.rel_target_loc == 'before') ) {
        if (!thisObj.prev)
            return {error: 'PREV_NOT_FOUND'}

        thisObj.this = thisObj.prev
        thisObj.prev = thisObj.pre_prev
    }
    else if ( deixis.deixis == 'next' || (deixis.granularity == 'sentence' && deixis.rel_target_loc == 'after') ) {
        if (!thisObj.next)
            return {error: 'NEXT_NOT_FOUND'}

        thisObj.prev = thisObj.this
        thisObj.this = thisObj.next
    }
    
    return {
        lbound: thisObj.prev +2 || 0,
        ubound: thisObj.this
    }

}

const navigateDir = function(query, thisObj, rel_loc, abs_loc) {
    var targetLocation; 
    formatString = 'default';

    if (abs_loc) {
        if (abs_loc == 'beginning') {
            targetLocation = thisObj.lbound;
            query = query.replace(query[0], query[0].toUpperCase())
            formatString = 'insertAtBeginning'
        }
        else if (abs_loc == 'end') {
            targetLocation = thisObj.ubound
            query = ' ' + query
            formatString = 'newline'
        }

    }

    else if (rel_loc) {
        if (rel_loc == 'after')
            targetLocation = thisObj.ubound + 2; 
        else if (rel_loc == 'before')
            targetLocation = thisObj.lbound;

        // query modification
        query = query.replace(query[0], query[0].toUpperCase())
        query = query + '.'
    }

    return {query, targetLocation, formatString}

}


// function match(data, locationContext) {
//     return new Promise(function(resolve) {
//         var l_context = getLeftContext(data, locationContext), 
//             r_context = getRightContext(data+' ', locationContext);

//         console.log('l_context', l_context);
//         console.log('r_context', r_context);

//         if ( (l_context.location != -1 && r_context.location != -1) && (r_context.location > l_context.location) ) {
//             resolve({
//                 type: 'L∂R',
//                 context: {left: l_context, right: r_context}
//             })
//         }

//         else if ( r_context.location != -1 ) {
//             resolve({
//                 type: '∂R',
//                 context: {left: -1, right: r_context}
//             })
//         }

//         else
//             resolve('default')

//     })
// }


function sendPacketToNLPServer(data, locationContext) {
        var sentenceAtLocationContext = getSentenceAtLocationContext(locationContext);
        var dataToAppend = data.trim().replace(data.charAt(0), data.charAt(0).toUpperCase())    // pre-process query: remove extra leading spaces and make sure first char is uppercase
        var dataToNLPServer = sentenceAtLocationContext + ' ' + dataToAppend 

        console.log('Data sent to NLP Server :: ', dataToNLPServer)

        socket.emit('analyze', {data: dataToNLPServer, locationContext});
}

export function onDataFromNLPServer(data) {
    return new Promise(function(resolve) {
        console.log('Data analysis received from NLP Server :: ', data)

        var delimiterIndex = data.findIndex(x => x.token === '.')
        var split_ = splitPacket(data, delimiterIndex);

        var targetTokens = split_[0],
            queryTokens  = split_[1]; 

        console.log('target tokens :: ', targetTokens)
        console.log('query tokens :: ', queryTokens)

        if (targetTokens && queryTokens)
            resolve(
                {
                    targetTokens,
                    queryTokens
                }
            )

    })
}

function splitPacket(data, index) {
    var targetTokens = data.slice(0, index)
    var queryTokens  = data.slice(index + 1) 
    queryTokens[0].token = queryTokens[0].token.charAt(0).toLowerCase() + queryTokens[0].token.slice(1)

    /* adjust headIndices for query */
    for ( var i = 0; i < queryTokens.length; i++ ) 
        queryTokens[i].headIndex = queryTokens[i].headIndex - (index + 1)

    return [targetTokens, queryTokens]
}


export function contextMatcher(tokens, locationContext) {
    return new Promise(async function(resolve) {
        console.log('inside contextMatcher', tokens); 
        
        var target = tokens.targetTokens, 
            query  = tokens.queryTokens; 

        var targetTree = buildtree(target)
        console.log('target tree :: ', targetTree)
            
        var queryTree = buildtree(query)
        console.log('query tree :: ', queryTree)


        // var indexOfConj = target.findIndex(x => x.label === 'CC')
        // if (indexOfConj >= 0)
        //     target = target.filter( (x,i) => i > indexOfConj )

        
        var tokenThreads = []
        query.forEach((token_, index) => {
            // priority matching based algorithm
            var value = -1; 
            value = target.findIndex( x => x.token.toLowerCase() === token_.token.toLowerCase() || x.stem === token_.stem ) // priority level : 1 (exact match (incl. stem))
            if ( value == -1 ) // priority level : 2
                value = target.findIndex( x => x.proper === 'PROPER' && token_.proper === 'PROPER'  || x.lemma === token_.lemma )
                if ( value == -1 )    // priority level : 3 (fuzzy match)
                    // value = target.findIndex( x => x.lexdomain && (x.lexdomain === token_.lexdomain) || sim(x.tag) === sim(token_.tag) && sim_(x.label) === sim_(token_.label) ) 
                    value = target.findIndex( x => x.lexdomain && (x.lexdomain === token_.lexdomain) ) 

            tokenThreads.push(
                {   // value -> index in targetTokens. index -> index in queryTokens
                    value,
                    index
                }
            )
        });


        /*

        var tokenThreads = await fuzzyTokenMatch(target, query)
        console.log('tokenThreads :: ', tokenThreads)   // cartesian product

        // get lis for all token threads and select best candidate
        var lisAllThreads = tokenThreads.map(x => longestIncreasingSequence(x, 'value', true))
        console.log('lisAllThreads :: ', lisAllThreads)

        // select best candidates <longest chain length>
        var chainLengths = lisAllThreads.map(x => x.length)
        console.log('all chain lengths :: ', chainLengths)

        var maxLength = Math.max.apply(Math, chainLengths);
        console.log('maxLength :: ', maxLength)

        var chosenThreads = lisAllThreads.filter(x => x.length == maxLength)
        console.log('chosen Threads :: ', chosenThreads)

        // var tokenDists = chosenThreads.map( (x,i) => ({dist: d3.pairs(x).reduce( (acc, pair) => acc + (pair[1].value - pair[0].value), 0 ), index: i}) )
        var tokenDists = lisAllThreads.map( (x,i) => ({dist: d3.pairs(x).reduce( (acc, pair) => acc + (pair[1].value - pair[0].value), 0 ), index: i}) )
        console.log('token distances :: ', tokenDists)
        
        var minDist = Math.min.apply(Math, tokenDists.map(x => x.dist))
        console.log('min. dist ', minDist)

        // choose the candidate with minimum distance (min. intra-cluster distance)
        var indexOfBest = tokenDists.filter(x => x.dist == minDist).pop()
        console.log('index of best :: ', indexOfBest)

        // var lis = chosenThreads[indexOfBest.index]
        var lis = lisAllThreads[indexOfBest.index]
        console.log('lis :: ', lis)

        */
       

        tokenThreads = tokenThreads.filter(x => x.value >= 0)
        console.log('tokenThreads :: ', tokenThreads)

        var lis = longestIncreasingSequence(tokenThreads, 'value')
        console.log('lis :: ', lis)


        if( !isValid(lis) || !isValid(lis[0]) || !lis.length )
            resolve(
                {
                    status: 'error', 
                    errorMessage: {
                        text: error(
                            {param: query.map(x => x.token).join(' ')}
                        )['NO_INTENT_MATCHED'],
                        rate: dynamicErrorMessageRate
                    }
                }
            )
        else {
            var leftSlot    = {}, 
                leftTarget  = {},
                matchTarget = {},
                matchQuery  = {},
                rightSlot   = {},
                rightTarget = {};

            leftSlot.tokens = query.filter( (x, index) => index < lis[0].index )
            leftSlot.text = leftSlot.tokens.map(x => x.token).join(' ')
            
            rightSlot.tokens = query.filter( (x, index) => index > lis[lis.length -1].index )
            rightSlot.text = rightSlot.tokens.map(x => x.token).join(' ')
            
            // query := slot + matching context 
            matchQuery.tokens = query.filter( (x, index) => index >= lis[0].index && index <= lis[lis.length -1].index )
            matchQuery.text = matchQuery.tokens.map(x => x.token).join(' ')
            
            matchTarget.tokens = target.filter( (x, index) => index >= lis[0].value && index <= lis[lis.length -1].value )
            matchTarget.text = matchTarget.tokens.map(x => x.token).join(' ')

            leftTarget.tokens = target.filter( (x, index) => index < lis[0].value )
            leftTarget.text = leftTarget.tokens.map(x => x.token).join(' ')
            
            rightTarget.tokens = target.filter( (x, index) => index > lis[lis.length -1].value )
            rightTarget.text = rightTarget.tokens.map(x => x.token).join(' ')

            console.log('leftSlot :: ', leftSlot)
            console.log('leftTarget :: ', leftTarget)
            console.log('rightSlot :: ', rightSlot)
            console.log('rightTarget :: ', rightTarget)
            console.log('matchQuery :: ', matchQuery)
            console.log('matchTarget :: ', matchTarget)

            if (matchQuery){
                var ret = await getQuillUpdateParams(matchTarget, matchQuery, leftTarget, leftSlot, rightTarget, rightSlot, locationContext)
                
                resolve(
                    {
                        intent: 'change',
                        pos: ret.pos,
                        nChar: ret.nChar,
                        slot: ret.slot,
                        formatString: ret.formatString,
                        ttsResumeAt: getResumeAtCharIndex(ret.pos)
                    }
                )
            }
        }
        
    });

}

const buildtree = (tokens) => {
    let tree = {}; 
    // tree.root = tokens.find(x => x.label === 'ROOT')
    tokens.forEach( (token_, index) =>  { 
        if (!tree[index]) {
            // console.log('Creating node for ', token_, ' at index ', index)
            tree[index] = {}
        }
        tree[index].father = tokens[token_.headIndex]; 
        // console.log('Assigned ', tokens[token_.headIndex], ' as father to ', token_)

        if ( index < token_.headIndex ) {
            if (!tree[token_.headIndex]) {
                // console.log('Creating node for ', tokens[token_.headIndex], ' at index ', token_.headIndex)
                tree[token_.headIndex] = {}
                tree[token_.headIndex].left = []
            }
            tree[token_.headIndex].left.push(tokens[index])
            // console.log('Pushed ', tokens[index], ' as a left child of ', tokens[token_.headIndex])

        }

        else if ( index > token_.headIndex ) {
            if (tree[token_.headIndex].right === undefined) 
                tree[token_.headIndex].right = []
            tree[token_.headIndex].right.push(tokens[index])
            // console.log('Pushed ', tokens[index], ' as a right child of ', tokens[token_.headIndex])
        }
            
    })

    return tree; 
}

const sim = (tag) => (tag === 'PRON') ?'NOUN' :tag
const sim_ = (label) => (label === 'NUMBER') ?'NUM' :label

const getQuillUpdateParams = (matchTarget, matchQuery, leftTarget, leftSlot, rightTarget, rightSlot, locationContext) => {  
    return new Promise(async resolve => {
        var absCharPos, relCharPos
        var LMScoreThreshold = LM_SCORE_THRESHOLD;

        var _2nn = sentIndexToCharIndexBounds(locationContext)
        
        if ( !isValid(locationContext) || locationContext <= 0 )
            absCharPos = 0
            else absCharPos = _2nn.prev +2 || 0
        console.log('absCharPos of sent. ', absCharPos)

        var sent = getSentenceAtLocationContext(locationContext);
        
        relCharPos = overlap.left(sent, matchTarget.text).location
        console.log('relCharPos of context within sent. ', relCharPos)
        
        absCharPos += relCharPos
        console.log('re-computed absCharpos :: ', absCharPos)

        // var slot = stripContext(matchTarget.text, matchQuery.text)        // slot := {text: , start:}
        var slot = {text: matchQuery.text, start: 0}
        console.log('slot object :: ', slot)

        // var pos = absCharPos + slot.start;
        var pos = absCharPos; 
        console.log('pos of change :: ', pos)

        var matchSlot = Object.assign({}, slot);    // matchSlot := {text: , start:}

        var  backShift   = 0,
            forwardShift = 0; 

        // var leftChildrenToRemove, rightChildrenToRemove; 

        if ( leftSlot.text.length ) {
            // slot text 
            console.log('leftSlot text :: ', leftSlot.text)
            
            slot.text = leftSlot.text + ' ' + slot.text
            console.log('new slot.text (after adding leftSlot text) :: ', slot.text)

            // consult Language Model for determining what part of leftTarget should be replaced by leftSlot
            var avgLMScore; 
            var i; 
            if (leftTarget.text.length) {
                var tokens = leftTarget.tokens
                for ( i = tokens.length - 1; i >= 0; i-- ) {
                    console.log('i :: ', i)
                    if ( isValid2Gram ( tokens[i], leftSlot.tokens[0] ) >=0 )
                        break;

                    var offset = tokens.length - i - 1;
                    // avgLMScore = await consultLM( tokens.slice(0, -offset || tokens.length), leftSlot.tokens, -offset ) 
                    avgLMScore = await consultLM(tokens, leftSlot.tokens, -offset) 
                    console.log(`average LM score for token ${i} = ${avgLMScore}`)
                    if ( avgLMScore > LMScoreThreshold )
                        break;
                }

                if ( i == -1 ) // replace the whole of the leftTarget with the leftSlot
                    backShift = tokens.map(x => x.token).join(' ').length +1
                else if (i == tokens.length - 1)
                    backShift = 0
                else
                    backShift = tokens.slice(i+1, tokens.length).map(x => x.token).join(' ').length +1
                
                console.log('computed backShift :: ', backShift)

                pos = absCharPos - backShift
                console.log('recomputed pos :: ', pos)
            }

        }   // end of leftSlot insertion

        if ( rightSlot.text.length ) {
            // slot text 
            console.log('rightSlot text :: ', rightSlot.text)

            slot.text = slot.text + ' ' + rightSlot.text
            console.log('new slot.text (after adding rightSlot text) :: ', slot.text)

            // consult Language Model for determining what part of rightTarget should be replaced by rightSlot
            var avgLMScore; 
            var i; 
            if (rightTarget.text.length) {
                var tokens = rightTarget.tokens
                for ( i = 0; i < tokens.length; i++ ) {
                    console.log('i :: ', i)
                    if ( isValid2Gram ( rightSlot.tokens[rightSlot.tokens.length -1], tokens[i] ) >=0 )
                        break;

                    var offset = i;
                    avgLMScore = await consultLM(tokens, rightSlot.tokens, offset, 'right') 
                    console.log(`average LM score for token ${i} = ${avgLMScore}`)
                    if ( avgLMScore > LMScoreThreshold )
                        break;
                }

                if ( i == 0 )   // no replacement of any rightTarget token, direct insertion
                    forwardShift = 0
                else if (i == tokens.length)    // replace the whole of the rightTarget with the rightSlot
                    forwardShift = tokens.map(x => x.token).join(' ').length +1
                else
                    forwardShift = tokens.slice(0, i).map(x => x.token).join(' ').length +1
                
                console.log('computed forwardShift :: ', forwardShift)

            }

            /*

            if ( isValid2Gram ( rigthSlot.tokens[0], rightTarget.tokens[0] ) == -1 ) {
                // starting pos
                if (!matchSlot.text.length && pos == absCharPos) {
                    pos += matchTarget.text.length;
                    console.log('recomputed absCharPos (after shifting over matchTarget text ::', absCharPos)
                }

                // determine nChar (no. of chars to delete)
                if ( rightTarget.text.length ) {
                    // if ( matchSlot.text.length ) {
                    //     // remove left Children of boundary token (of target) replaced 
                    //     var index = leftTarget.tokens.length + matchTarget.tokens.length -1
                    //     if (targetTree[index].right !== undefined) {
                    //         rightChildrenToRemove = targetTree[index].right.map(x => x.token)
                    //         if ( rightChildrenToRemove.length ) 
                    //             forwardShift = rightChildrenToRemove.join(' ').length +1
                    //     }
                    // }
                    // console.log('forward shift after deleting right children of replaced boundary word of matchTarget :: ', forwardShift)

                    var tokenMarkedForDeletion = rightTarget.tokens.map(x => false)
                    var fuzzyMatchIndices = [];

                    var baseIndexOfSlotToken    = leftSlot.tokens.length + matchQuery.tokens.length
                    var baseIndexOfTargetToken  = leftTarget.tokens.length + matchTarget.tokens.length
                    var absIndexOfSlotToken, absIndexOfTargetToken;

                    var ccIndex = rightTarget.tokens.findIndex(x => x.label === 'CC') 
                    var rightTargetSplitAtCC = rightTarget.tokens.filter((x,i) => i < ccIndex)
                    console.log('rightTarget Split At CC ', rightTargetSplitAtCC)

                    for (var i=0; i < rightSlot.tokens.length; i++) {
                        var x = rightSlot.tokens[i]
                        absIndexOfSlotToken = baseIndexOfSlotToken + i;

                        for ( var j=0; j < rightTargetSplitAtCC.length; j++ ) {
                            var token_ = rightTargetSplitAtCC[j]
                            absIndexOfTargetToken = baseIndexOfTargetToken + j;

                            // console.log('sim(x.tag) :: ', sim(x.tag))
                            // console.log('sim(token_.tag) :: ', sim(token_.tag))
                            // console.log('sim_(x.label) :: ', sim_(x.label))
                            // console.log('sim_(token_.label) :: ', sim_(token_.label))
                            // console.log('targetTree[absIndexOfTargetToken].father :: ', targetTree[absIndexOfTargetToken].father)
                            // console.log('queryTree[absIndexOfSlotToken].father :: ', queryTree[absIndexOfSlotToken].father)

                            if ( ( ( sim(x.tag) === sim(token_.tag) && sim_(x.label) === sim_(token_.label) ) 
                                    || targetTree[absIndexOfTargetToken].father.token === queryTree[absIndexOfSlotToken].father.token )                            
                                    && !tokenMarkedForDeletion[j] ) {
                                fuzzyMatchIndices.push(j);
                                tokenMarkedForDeletion[j] = true;
                                // break;
                            }
                        }

                        console.log(`fuzzyMatchIndices ${i} :: ${fuzzyMatchIndices}`)
                    }
                    
                    console.log('fuzzyMatchIndices :: ', fuzzyMatchIndices)

                    if (fuzzyMatchIndices.length) {
                        var relLastIndex = fuzzyMatchIndices.pop()  // rel. last index of target to be deleted
                        // var absLastIndex = leftTarget.tokens.length + matchTarget.tokens.length + relLastIndex
                        
                        // if (targetTree[absLastIndex].right !== undefined) {
                        //     rightChildrenToRemove = targetTree[absLastIndex].right.map(x => x.token)
                        //     if ( rightChildrenToRemove.length ) 
                        //         forwardShift += rightChildrenToRemove.join(' ').length +1
                        // }
                        // console.log('forward shift after deleting right children of replaced boundary word of rightTarget :: ', forwardShift)

                        forwardShift += rightTarget.tokens.filter( (x,i) => i <= relLastIndex).map(x => x.token).join(' ').length +1
                        console.log('total forward shift :: ', forwardShift)

                    }

                }
            } 

            */

        }   // end of rightSlot insertion

        
        var nChar = matchTarget.text.length - (matchQuery.text.length - matchSlot.text.length) + backShift + forwardShift;
        var formatString_ = 'default'; 

        console.log(pos + nChar)
        console.log('charAt quill.getText(pos + nChar, 1) :: ', quill.getText(pos + nChar, 1))
        
        if (quill.getText(pos + nChar, 1) === '.') {
            slot.text = slot.text + '.'
            formatString_ = 'endOfSentence'
        }
        
        if (pos == 0 || quill.getText(pos -2, 1) === '.')
            slot.text = slot.text.charAt(0).toUpperCase() + slot.text.slice(1)

        var re = /\s(?!\b)/g
        slot.text = slot.text.replace(re, '')

        if (slot.text.trim() == '')
            formatString_ = 'noupdate'

        nChar++;

        resolve(
            {
                pos,
                nChar,
                slot: slot.text,
                formatString: formatString_
            }
        )

    }); // end of Promise
    
}   // end of function

const stripContext = (target, query) => {
    if (target === query)
        return {
            text: '',
            start: 0
        }

    var left = overlap.left(query, target)
    var right = overlap.right(query, target)

    console.log ('left :: ', left)
    console.log ('right :: ', right)
    
    var start, end; 
    if (left.location >= 0)
        start = left.location + left.content.length +1
        else start = 0;

    if (right.location >= 0)
        end = right.location
        else end = query.length
    
    if (start > query.length)
        return {
            text: '',
            start: 0
        }

    return {
        text: query.slice(start, end).trim(),
        start
    }
}

const fuzzyTokenMatch = (target, query) => {
    return new Promise(function(resolve) {
        var tokenChainHash = []  // [ [ {} ] ] := array of (array of objects with 2 keys {value:= target index, index:= query indes})
        
        tokenChainHash = query.reduce((chainTokens, token_, index) => {
            // priority matching based algorithm
            var match = []

            // priority level : 1 (exact match (incl. stem & lemma))
            match = target.reduce((acc, x, i)  => {
                if ( x.token.toLowerCase() === token_.token.toLowerCase() || x.stem === token_.stem || x.lemma === token_.lemma )
                    acc.push({value: i, index})
                return acc
            }, []);

            if ( !match.length ) // priority level : 2
                match = target.reduce((acc, x, i)  => {
                    if ( x.proper === 'PROPER' && token_.proper === 'PROPER' ) 
                        acc.push({value: i, index})
                    return acc
                }, []);
                
                if ( !match.length )    // priority level : 3 (fuzzy match)
                    match = target.reduce((acc, x, i)  => {
                        if ( x.lexdomain && (x.lexdomain === token_.lexdomain) || sim(x.tag) === sim(token_.tag) && sim_(x.label) === sim_(token_.label) ) 
                            acc.push({value: i, index})
                        return acc
                    }, []);

            
            if (match.length)
                chainTokens.push(match)

            return chainTokens

        }, []);

        console.log('tokenChainHash :: ', tokenChainHash)

        resolve( cartesian(...tokenChainHash) )
    })
    
}


const consultLM = (target, slot, offset, dir) => {   // offset is a signed integer (-ve for leftTarget)
    dir = dir || 'left'
    console.log('consulting LM for direction :: ', dir)

    console.log('inside consultLM')
    console.log('target received :: ', target)
    console.log('slot received :: ', slot)
    console.log('offset received :: ', offset)

    var isPossible_3gram_1 = true
    var isPossible_3gram_2 = true

    if ( dir == 'left' ) {
        if ( !target[target.length -1 + offset] || !slot[0] )
            resolve(0)

        if ( !target[target.length -2 + offset] )
            isPossible_3gram_1 = false

        if ( !slot[1] )
            isPossible_3gram_2 = false

    } else if ( dir == 'right' ) {
        if ( !slot[slot.length - 1] || !target[offset] )
            resolve(0)

        if ( !slot[slot.length - 2] )
            isPossible_3gram_1 = false

        if ( !target[offset + 1] )
            isPossible_3gram_2 = false
    }

    return new Promise(resolve => {
        let LMQuery = {}

        LMQuery._2gram = {}
        LMQuery._3gram = {}
        LMQuery._3gram._1 = {}
        LMQuery._3gram._2 = {}

        LMQuery._2gram.tokens = []
        LMQuery._3gram._1.tokens = []
        LMQuery._3gram._2.tokens = []

        if ( dir == 'left') {
            LMQuery._2gram.tokens.push(target[target.length -1 + offset])
            LMQuery._2gram.tokens.push(slot[0])

            if (isPossible_3gram_1)
                LMQuery._3gram._1.tokens.push(target[target.length -2 + offset])
                LMQuery._3gram._1.tokens.push(target[target.length -1 + offset])
                LMQuery._3gram._1.tokens.push(slot[0])

            if (isPossible_3gram_2)
                LMQuery._3gram._2.tokens.push(target[target.length -1 + offset])
                LMQuery._3gram._2.tokens.push(slot[0])
                LMQuery._3gram._2.tokens.push(slot[1])

        } else if ( dir == 'right' ) {
            LMQuery._2gram.tokens.push(slot[slot.length - 1])
            LMQuery._2gram.tokens.push(target[offset])

            if (isPossible_3gram_1)
                LMQuery._3gram._1.tokens.push(slot[slot.length - 2])
                LMQuery._3gram._1.tokens.push(slot[slot.length - 1])
                LMQuery._3gram._1.tokens.push(target[offset])

            if (isPossible_3gram_2)
                LMQuery._3gram._2.tokens.push(slot[slot.length - 1])
                LMQuery._3gram._2.tokens.push(target[offset])
                LMQuery._3gram._2.tokens.push(target[offset + 1])
        }

        console.log('LMQuery._2gram :: ', LMQuery._2gram)
        console.log('LMQuery._3gram :: ', LMQuery._3gram)
        
        LMQuery._2gram.text = LMQuery._2gram.tokens.map(x => x.token).join('+')

        if (isPossible_3gram_1)
            LMQuery._3gram._1.text = LMQuery._3gram._1.tokens.map(x => x.token).join('+')

        if (isPossible_3gram_2)
            LMQuery._3gram._2.text = LMQuery._3gram._2.tokens.map(x => x.token).join('+')

        const queryLM_2gram = (queryString) => {
            return new Promise(async resolve => {
                if (!queryString)
                    resolve(undefined)
                else {
                    var LMScoreObj
                    try {
                        LMScoreObj = await queryLM(queryString)
                    } catch (err) {
                        LMScoreObj = await queryLM(queryString)
                    }

                    var LMScore; 
                    try {
                        LMScore = JSON.parse(LMScoreObj).phrases[0].mc
                    } catch(TypeError) {
                        LMScore = 0
                    }

                    resolve(LMScore)
                }
            }) 
        }

        const queryLM_3gram_1 = (queryString) => {
            return new Promise(async resolve => {
                if (!isPossible_3gram_1)
                    resolve(undefined)
                else {
                    var LMScoreObj
                    try {
                        LMScoreObj = await queryLM(queryString)
                    } catch (err) {
                        LMScoreObj = await queryLM(queryString)
                    }

                    var LMScore; 
                    try {
                        LMScore = JSON.parse(LMScoreObj).phrases[0].mc
                    } catch(TypeError) {
                        LMScore = 0
                    }
                    
                    resolve(LMScore)
                }
            }) 
        }
        
        const queryLM_3gram_2 = (queryString) => {
            return new Promise(async resolve => {
                if (!isPossible_3gram_2)
                    resolve(undefined)
                else {
                    var LMScoreObj
                    try {
                        LMScoreObj = await queryLM(queryString)
                    } catch (err) {
                        LMScoreObj = await queryLM(queryString)
                    }

                    var LMScore; 
                    try {
                        LMScore = JSON.parse(LMScoreObj).phrases[0].mc
                    } catch(TypeError) {
                        LMScore = 0
                    }
                    
                    resolve(LMScore)
                }
            }) 
        }

        Promise.all( [ queryLM_2gram(LMQuery._2gram && LMQuery._2gram.text || undefined), queryLM_3gram_1(isPossible_3gram_1 && LMQuery._3gram._1.text || undefined), queryLM_3gram_2(isPossible_3gram_2 && LMQuery._3gram._2.text || undefined) ] )
            .then( (scores) => {
                console.log('raw scores array :: ', scores)
                scores = scores.filter(x => x >= 0)
                console.log('scores array after filtering undefined :: ', scores)
                var penaltyFactor = 1; 
                var totalScore = scores.reduce( function(sum, a) { 
                    if (a == 0)
                        penaltyFactor *= PENALTY_FACTOR
                    return (sum + a) 
                }, 0 )

                console.log('∑ scores = ', totalScore)
                console.log('Penalty Factor = ', penaltyFactor)

                var totalScoreWithPenalty; 
                try {
                    totalScoreWithPenalty = totalScore / (scores.length * penaltyFactor)

                } catch(err) {
                    totalScoreWithPenalty = 0
                }

                if (totalScoreWithPenalty >= 0)
                    resolve(totalScoreWithPenalty)

            })
        
    })
}
