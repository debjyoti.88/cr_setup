export function ForwardNeg(tokenList) {    // token can have only 1 forward negative next stop
    var nextStop = {};

    for ( var i = 0; i < tokenList.length; i++ ) {
        var index = tokenList[i].headIndex;
        // console.log('i', i, ' :: index', index)
        if  ( index < i ) {
            nextStop[i] = {}
            nextStop[i].index = index
            nextStop[i].blocked = false;
        }
    }

    return nextStop
}

export function ForwardPos(tokenList) {    // token can have multiple forward positive next stop
    var nextStop = {};

    for ( var i = 0; i < tokenList.length; i++ ) {
        var index = tokenList[i].headIndex;
        // console.log('i', i, ' :: index', index)
        if  ( index < i ) {
            if ( nextStop[index] === undefined ) {
                console.log(`creating a new key ${index}`)
                nextStop[index] = new Array();
            }
        
            nextStop[index].push({index: i, blocked: false})
        }
    }

    return nextStop
}

export function BackwardNeg(tokenList) {   // token can have multiple backward negative next stop
    var nextStop = {};

    for ( var i = 0; i < tokenList.length; i++ ) {
        var index = tokenList[i].headIndex;
        // console.log('i', i, ' :: index', index)
        if  ( index > i ) {
            if ( nextStop[index] === undefined ) {
                console.log(`creating a new key ${index}`)
                nextStop[index] = new Array();
            }
        
            nextStop[index].push({index: i, blocked: false})
        }
    }

    return nextStop
}

export function BackwardPos(tokenList) {   // token can have only 1 backward positive next stop
    var nextStop = {};

    for ( var i = 0; i < tokenList.length; i++ ) {
        var index = tokenList[i].headIndex;
        // console.log('i', i, ' :: index', index)
        if  ( index > i ) {
            nextStop[i] = {}
            nextStop[i].index = index
            nextStop[i].blocked = false;
        }
    }

    return nextStop
}

